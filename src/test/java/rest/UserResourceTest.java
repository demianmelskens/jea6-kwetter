package rest;

import dao.UserDAO;
import org.junit.Before;
import org.junit.Test;
import services.UserService;

import javax.inject.Inject;

public class UserResourceTest {

    @Inject
    UserDAO userDAO;

    @Inject
    UserService userService;


    @Before
    public void setUp() throws Exception {
    }


    public void testUserCount() {
    }

    @Test
    public void addUser() {
    }

    @Test
    public void editUser() {
    }

    @Test
    public void deleteUser() {
    }

    @Test
    public void getUser1() {
    }

    @Test
    public void getUserByName() {
    }

    @Test
    public void getAllUsers() {
    }

    @Test
    public void getFollowers() {
    }

    @Test
    public void getFollowing() {
    }
}