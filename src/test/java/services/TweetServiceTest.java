package services;

import dao.TweetDAO;
import domain.Tweet;
import domain.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import util.enums.UserType;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@RunWith(MockitoJUnitRunner.class)
public class TweetServiceTest {

    private TweetService tweetService;
    private Tweet tweet;
    private User user1;
    private User user2;

    @Mock
    private TweetDAO tweetDAO;

    @Before
    public void setUp() throws Exception {
        tweetService = new TweetService();
        tweetService.tweetDAO = this.tweetDAO;
        this.user1 = new User("TestUser", "secret", "Hallo dit is mijn eerste bio", "Eindhoven", "www.test.com");
        this.user2 = new User("TestUser2", "secret2", "Hallo dit is mijn eerste bio2", "Eindhoven2", "www.test.com2");
        this.tweet = new Tweet("Test Tweet", this.user1, new Date());
    }

    @Test
    public void testCreateTweet() {
        tweetService.createTweet(this.tweet);
        Mockito.verify(tweetDAO, Mockito.times(1)).create(this.tweet);
    }

    @Test
    public void testRemove() {
        tweetService.createTweet(this.tweet);
        tweetService.remove(this.tweet);
        Mockito.verify(tweetDAO, Mockito.times(1)).remove(this.tweet);
    }

    @Test
    public void testFindTweet() {
        tweetService.findTweet(1L);
        Mockito.verify(tweetDAO, Mockito.times(1)).find(1L);
    }

    @Test
    public void testFindTweetsByDate() throws ParseException {
        Date date = this.tweet.getPostedAt();
        DateFormat format = new SimpleDateFormat("YYYY-MM-DD HH:MM:SS", Locale.ENGLISH);
        tweetService.findTweetsByDate(format.format(date));
        date = format.parse(format.format(date));
        Mockito.verify(tweetDAO, Mockito.times(1)).findByDate(date);
    }

    @Test
    public void testGetTweetsForUser() {
        tweetService.getTweetsForUser(this.user1);
        Mockito.verify(tweetDAO, Mockito.times(1)).getTweetsForUser(this.user1);
    }

    @Test
    public void testFindAllTweets() {
        tweetService.findAllTweets();
        Mockito.verify(tweetDAO, Mockito.times(1)).findAll();
    }

    @Test
    public void testGetTimelineForUser() {
        tweetService.getTimelineForUser(this.user1);
        Mockito.verify(tweetDAO, Mockito.times(1)).getTimelineForUser(this.user1);
    }

    @Test
    public void testLike(){
        tweetService.likeTweet(this.tweet, this.user2);
        Mockito.verify(tweetDAO, Mockito.times(1)).like(this.tweet, this.user2);
    }

    @Test
    public void testUnlike(){
        tweetService.unlikeTweet(this.tweet, this.user2);
        Mockito.verify(tweetDAO, Mockito.times(1)).unlike(this.tweet, this.user2);
    }
}