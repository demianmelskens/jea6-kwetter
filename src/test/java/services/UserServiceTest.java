package services;

import dao.UserDAO;
import domain.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import util.enums.UserType;
import util.generators.AuthGenerator;

import java.util.Date;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserService userService;
    private User user1;
    private User user2;

    @Mock
    private UserDAO userDAO;

    @Before
    public void setUp() throws Exception {
        userService = new UserService();
        userService.userDAO = this.userDAO;
        this.user1 = new User("TestUser", "secret", "Hallo dit is mijn eerste bio", "Eindhoven", "www.test.com");
        this.user2 = new User("TestUser2", "secret2", "Hallo dit is mijn eerste bio2", "Eindhoven", "www.test.com");
        userService.authUser = this.user1;
    }

    @Test
    public void testCount() {
        userService.count();
        Mockito.verify(userDAO, Mockito.times(1)).count();
    }

    @Test
    public void testCreateUser() {
        userService.createUser(this.user1);
        Mockito.verify(userDAO, Mockito.times(1)).create(this.user1);
    }

    @Test
    public void testEditUser() {
        userService.createUser(this.user1);
        this.user1.setName("UserTest");
        userService.editUser(this.user1);
        Mockito.verify(userDAO, Mockito.times(1)).edit(this.user1);
    }

    @Test
    public void testRemoveUser() {
        userService.createUser(this.user1);
        userService.removeUser(this.user1);
        Mockito.verify(userDAO, Mockito.times(1)).remove(this.user1);
    }

    @Test
    public void testFindUser() {
        userService.findUser(1L);
        Mockito.verify(userDAO, Mockito.times(1)).find(1L);
    }

    @Test
    public void testFindByName() {
        userService.findByName("TestUser");
        Mockito.verify(userDAO, Mockito.times(1)).findByName("TestUser");
    }

    @Test
    public void testFindAllUsers() {
        userService.findAllUsers();
        Mockito.verify(userDAO, Mockito.times(1)).findAll();
    }

    @Test
    public void testFollowUser() {
        userService.createUser(this.user1);
        userService.createUser(this.user2);
        userService.followUser(this.user2);
        Mockito.verify(userDAO, Mockito.times(1)).followUser(this.user1, this.user2);
    }

    @Test
    public void testUnfollowUser() {
        userService.createUser(this.user1);
        userService.createUser(this.user2);
        userService.unfollowUser(this.user2);
        Mockito.verify(userDAO, Mockito.times(1)).unfollowUser(this.user1, this.user2);
    }

    @Test
    public void testGetFollowers() {
        userService.createUser(this.user1);
        userService.createUser(this.user2);
        userService.followUser(this.user2);
        userService.getFollowers(this.user2);
        Mockito.verify(userDAO, Mockito.times(1)).getFollowers(this.user2);
    }

    @Test
    public void testGetFollowing() {
        userService.createUser(this.user1);
        userService.createUser(this.user2);
        userService.followUser(this.user2);
        userService.getFollowing(this.user1);
        Mockito.verify(userDAO, Mockito.times(1)).getFollowing(this.user1);
    }

    @Test
    public void testLogin(){
        Mockito.when(userDAO.findByName(this.user1.getName())).thenReturn(this.user1);
        Date now = new Date(System.currentTimeMillis());
        String expected = AuthGenerator.generateToken(this.user1, now);
        Assert.assertEquals(expected,userService.login(this.user1, now));
        Mockito.verify(userDAO, Mockito.times(1)).findByName(this.user1.getName());
    }
}