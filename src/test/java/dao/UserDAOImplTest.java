package dao;

import domain.User;
import org.junit.*;
import util.DatabaseCleaner;
import util.enums.UserType;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

public class UserDAOImplTest {

    private UserDAOImpl userDAO;
    private User user1;
    private User user2;
    private User user3;

    private EntityManager em;
    private DatabaseCleaner databaseCleaner;

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "KwetterTestPU" );
        this.em = entityManagerFactory.createEntityManager();
        userDAO = new UserDAOImpl();
        userDAO.em = this.em;
        this.databaseCleaner = new DatabaseCleaner(this.em);

        this.user1 = new User("TestUser", "secret", "Hallo dit is mijn eerste bio", "Eindhoven", "www.test.com");
        this.user2 = new User("TestUser2", "secret", "Hallo dit is mijn eerste bio2", "Eindhoven", "www.test2.com");
        this.user3 = new User("TestUser3", "secret", "Hallo dit is mijn eerste bio3", "Eindhoven", "www.test3.com");

        this.em.getTransaction().begin();
        this.em.persist(this.user1);
        this.em.persist(this.user2);
        this.em.persist(this.user3);
        this.em.flush();
    }

    @After
    public void tearDown() throws SQLException {
        this.databaseCleaner.clean();
    }

    @Test
    public void testCount() {
        int expected = userDAO.count();
        int actual = this.em.createNamedQuery("User.count", Long.class).getSingleResult().intValue();
        this.em.getTransaction().commit();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCreate() {
        User expected = new User("TestUserCreate", "secret", "Hallo dit is mijn eerste bio Create", "Eindhoven", "www.test2.com");
        userDAO.create(expected);
        this.em.getTransaction().commit();
        User actual = this.em.createNamedQuery("User.findByName", User.class).setParameter("name", "TestUserCreate").getSingleResult();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testEdit() {
        this.user1.setBio("deze bio is aangepast");
        userDAO.edit(this.user1);
        this.em.getTransaction().commit();
        User actual = this.em.createNamedQuery("User.findByName", User.class).setParameter("name", "TestUser").getSingleResult();
        Assert.assertEquals(this.user1, actual);
    }

    @Test(expected = NoResultException.class)
    public void testRemove() {
        userDAO.remove(this.user3);
        this.em.getTransaction().commit();
        User actual = this.em.createNamedQuery("User.findByName", User.class).setParameter("name", "TestUser3").getSingleResult();
    }

    @Test
    public void testFind() {
        User actual = userDAO.find(this.user1.getId());
        this.em.getTransaction().commit();
        Assert.assertEquals(this.user1, actual);
    }

    @Test
    public void testFindByName() {
        User actual = userDAO.findByName("TestUser");
        this.em.getTransaction().commit();
        Assert.assertEquals(this.user1, actual);
    }

    @Test
    public void testFindAll() {
        Collection<User> expected = new ArrayList<User>();
        User user4 = new User("TestUser4", "secret", "Hallo dit is mijn eerste bio4", "Eindhoven", "www.test4.com");
        em.persist(user4);
        User user5 = new User("TestUser5", "secret", "Hallo dit is mijn eerste bio5", "Eindhoven", "www.test5.com");
        em.persist(user5);
        em.getTransaction().commit();

        expected.add(this.user1);
        expected.add(this.user2);
        expected.add(this.user3);
        expected.add(user4);
        expected.add(user5);

        Collection<User> actual = userDAO.findAll();
        Assert.assertEquals(expected.size(), actual.size());
    }

    @Test
    public void testFollowUser() {
        User actual = userDAO.followUser(this.user1, this.user2);
        em.getTransaction().commit();

        Assert.assertEquals(this.user2, actual);
    }

    @Test
    public void testunFollowUser() {
        User actual = userDAO.followUser(this.user1, this.user2);
        em.getTransaction().commit();

        Assert.assertEquals(this.user2, actual);
    }

    @Test
    public void testGetFollowers() {
        Collection<User> expected = new ArrayList<User>();
        userDAO.followUser(this.user1, this.user2);
        em.getTransaction().commit();

        expected.add(this.user1);
        Collection<User> actual = userDAO.getFollowers(this.user2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetFollowing() {
        Collection<User> expected = new ArrayList<User>();
        userDAO.followUser(this.user1, this.user2);
        em.getTransaction().commit();

        expected.add(this.user2);
        Collection<User> actual = userDAO.getFollowing(this.user1);
        Assert.assertEquals(expected, actual);
    }
}