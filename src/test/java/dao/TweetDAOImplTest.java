package dao;

import domain.Tweet;
import domain.User;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public class TweetDAOImplTest {

    private TweetDAOImpl tweetDAO;
    private User user1;
    private User user2;
    private Tweet tweet1;
    private Tweet tweet2;

    private EntityManager em;
    private DatabaseCleaner databaseCleaner;

    @Before
    public void setUp() throws Exception {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "KwetterTestPU" );
        em = entityManagerFactory.createEntityManager();
        tweetDAO = new TweetDAOImpl();
        tweetDAO.em = em;
        this.databaseCleaner = new DatabaseCleaner(this.em);

        this.user1 = new User("TestUser", "secret", "Hallo dit is mijn eerste bio", "Eindhoven", "www.test.com");
        this.user2 = new User("TestUser2", "secret", "Hallo dit is mijn eerste bio2", "Eindhoven", "www.test2.com");
        this.tweet1 = new Tweet("Tweet test", this.user1, new Date());
        this.tweet2 = new Tweet("Tweet test2", this.user2, new Date());

        this.em.getTransaction().begin();
        this.user1.addFollowing(this.user2);
        this.em.persist(this.user1);
        this.em.persist(this.user2);
        this.em.persist(this.tweet1);
        this.em.persist(this.tweet2);
        this.em.flush();
    }

    @After
    public void tearDown() throws SQLException {
        this.databaseCleaner.clean();
    }

    @Test
    public void testCreate() {
        User user3 = new User("TestUser3", "secret", "Hallo dit is mijn eerste bio", "Eindhoven", "www.test.com");
        this.em.persist(user3);
        Tweet expected = new Tweet("Tweet test", user3, new Date());
        tweetDAO.create(expected);
        this.em.getTransaction().commit();

        Tweet actual = em.createNamedQuery("Tweet.findById", Tweet.class).setParameter("id", expected.getId()).getSingleResult();
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NoResultException.class)
    public void testRemove() {
        User user4 = new User("TestUser4", "secret", "Hallo dit is mijn eerste bio", "Eindhoven", "www.test.com");
        this.em.persist(user4);
        Tweet expected = new Tweet("Tweet test", user4, new Date());
        em.persist(expected);
        tweetDAO.remove(expected);
        this.em.getTransaction().commit();

        Tweet actual = em.createNamedQuery("Tweet.findById", Tweet.class).setParameter("id", expected.getId()).getSingleResult();
    }

    @Test
    public void testFind() {
        Tweet actual = tweetDAO.find(this.tweet1.getId());
        this.em.getTransaction().commit();
        Assert.assertEquals(this.tweet1, actual);
    }

    @Test
    public void testFindByDate() {
        Collection<Tweet> actual = tweetDAO.findByDate(this.tweet1.getPostedAt());

        this.em.getTransaction().commit();
        Assert.assertThat(actual, IsCollectionContaining.hasItems(this.tweet1, this.tweet2));
    }

    @Test
    public void testGetTweetsForUser() {
        Collection<Tweet> actual = tweetDAO.getTweetsForUser(this.user1);

        this.em.getTransaction().commit();
        Assert.assertThat(actual, IsCollectionContaining.hasItems(this.tweet1));
    }

    @Test
    public void testFindAll() {
        Collection<Tweet> actual = tweetDAO.findAll();
        this.em.getTransaction().commit();

        Assert.assertThat(actual, IsCollectionContaining.hasItems(this.tweet1, this.tweet2));
    }

    @Test
    public void testGetTimelineForUser() {
        Collection<Tweet> actual = tweetDAO.getTimelineForUser(this.user1);

        this.em.getTransaction().commit();
        Assert.assertThat(actual, IsCollectionContaining.hasItems(this.tweet2));

        this.em.getTransaction().begin();
        this.user1.removeFollowing(this.user2);
        this.em.persist(this.user1);

        Collection<Tweet> actual2 = tweetDAO.getTimelineForUser(this.user1);

        this.em.getTransaction().commit();
        Assert.assertTrue(actual2.isEmpty());
    }

    @Test
    public void testLike() {
        Tweet tweet = tweetDAO.like(this.tweet1, this.user2);

        this.em.getTransaction().commit();
        Assert.assertThat(tweet.getLikes(), IsCollectionContaining.hasItems(this.user2));
    }

    @Test
    public void testUnlike() {
        Tweet tweet = tweetDAO.like(this.tweet1, this.user2);
        Tweet tweet2 = tweetDAO.unlike(this.tweet1, this.user2);

        this.em.getTransaction().commit();
        Assert.assertTrue(tweet.getLikes().isEmpty());
    }
}