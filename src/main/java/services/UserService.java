package services;

import authentication.annotations.AuthenticatedUser;
import util.enums.UserType;
import util.generators.AuthGenerator;
import dao.UserDAO;
import domain.User;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import util.converters.Hasher;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.security.Key;
import java.util.Collection;
import java.util.Date;

@Stateless
public class UserService {

    @Inject
    UserDAO userDAO;

    @Inject
    @AuthenticatedUser
    User authUser;

    /**
     * gets the total ammount of users in de database
     * @return int
     */
    public int count(){
        return userDAO.count();
    }

    /**
     * adds a new user to the database
     * @param user
     * @return User/null
     */
    public User createUser(User user){
        if(userDAO.find(user.getId()) == null) {
            return userDAO.create(user);
        }else{
            return null;
        }
    }

    /**
     * changes user information
     * @param user
     * @return User
     */
    public User editUser(User user){
        if(userDAO.find(user.getId()) != null) {
            return userDAO.edit(user);
        }else{
            return null;
        }
    }

    /**
     * deletes a user from the database based on the id
     * @param user
     * @return User
     */
    public User removeUser(User user){
        if(userDAO.find(user.getId()) != null) {
            return userDAO.remove(user);
        }else{
            return null;
        }
    }

    /**
     * gets a user from the database based on the id
     * @param id
     * @return User
     */
    public User findUser(long id){
        return userDAO.find(id);
    }

    /**
     * gets a user from the database based on the name
     * @param name
     * @return User
     */
    public User findByName(String name){
        return userDAO.findByName(name);
    }

    /**
     * get authenticated user
     * @return
     */
    public User getAuthenticatedUser(){
        return userDAO.find(authUser.getId());
    }

    /**
     * gets all users from the database
     * @return Collection<User>
     */
    public Collection<User> findAllUsers(){
        return userDAO.findAll();
    }

    /**
     * adds a user to the following list
     * @param user
     */
    public User followUser(User user) {
        return userDAO.followUser(this.authUser, user);
    }

    /**
     * removes a user from the following list
     * @param user
     */
    public User unfollowUser(User user) {
        return userDAO.unfollowUser(this.authUser, user);
    }

    /**
     * gets all the followers from the user based on the id
     * @param user
     * @return Collection<User>
     */
    public Collection<User> getFollowers(User user) {
        return userDAO.getFollowers(user);
    }

    /**
     * gets all the following from the user based on the id
     * @param user
     * @return Collection<User>
     */
    public Collection<User> getFollowing(User user) {
        return userDAO.getFollowing(user);
    }

    /**
     * Authenticate user
     * @param user
     * @param issuedAt
     * @return String
     */
    public String login(User user, Date issuedAt) {
        return AuthGenerator.generateToken(user, issuedAt);
    }

    /**
     * changes the rights of a user.
     * @param user
     * @return User
     */
    public User changeRights(User user, UserType type) {
        user.setType(type);
        return userDAO.edit(user);
    }

    /**
     * gets all the users which name look like input string
     * @param name
     * @return
     */
    public Collection<User> findSimilarUsers(String name){
        return this.userDAO.findSimilarUsers(name);
    }
}
