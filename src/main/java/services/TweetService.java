package services;

import dao.TweetDAO;
import domain.Tweet;
import domain.User;
import javafx.scene.chart.PieChart;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

@Stateless
public class TweetService{

    @Inject
    TweetDAO tweetDAO;

    /**
     * Adds a new tweet to the database
     * @param tweet
     * @return Tweet
     */
    public Tweet createTweet(Tweet tweet){
        return tweetDAO.create(tweet);
    }

    /**
     * deletes a tweet from the database based on id
     * @param tweet
     * @return Tweet
     */
    public Tweet remove(Tweet tweet){
        if(tweetDAO.find(tweet.getId()) != null) {
            return tweetDAO.remove(tweet);
        }else{
            return null;
        }
    }

    /**
     * gets a tweet from the database based on the id
     * @param id
     * @return Tweet
     */
    public Tweet findTweet(long id){
        return tweetDAO.find(id);
    }

    /**
     * gets tweets from the database based on the postedAt date
     * @param date
     * @return Collection<Tweet>
     */
    public Collection<Tweet> findTweetsByDate(String date) throws ParseException {
        DateFormat format = new SimpleDateFormat("YYYY-MM-DD HH:MM:SS", Locale.ENGLISH);
        return tweetDAO.findByDate(format.parse(date));
    }

    /**
     * gets all the tweets from a user
     * @param user
     * @return Collection<Tweet>
     */
    public Collection<Tweet> getTweetsForUser(User user){
        return tweetDAO.getTweetsForUser(user);
    }

    /**
     * gets all the tweets from the database
     * @return Collection<Tweet>
     */
    public Collection<Tweet> findAllTweets() {
        return tweetDAO.findAll();
    }

    /**
     * gets all the tweets in the timeline from the user
     * @param user
     * @return Collection<Tweet>
     */
    public Collection<Tweet> getTimelineForUser(User user) {
        return tweetDAO.getTimelineForUser(user);
    }

    /**
     * users is added to tweet likes
     * @param tweet tweet to like
     * @param user user who likes the tweet
     * @return Tweet
     */
    public Tweet likeTweet(Tweet tweet, User user) {
        return tweetDAO.like(tweet, user);
    }

    /**
     * user is removed from tweet likes
     * @param tweet tweet to unlike
     * @param user user who unlikes the tweet
     * @return Tweet
     */
    public Tweet unlikeTweet(Tweet tweet, User user) {
        return tweetDAO.unlike(tweet, user);
    }

    public Collection<Tweet> findSimilarTweets(String text) {
        return tweetDAO.findSimilarTweets(text);
    }
}
