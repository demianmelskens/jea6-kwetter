package authentication.producers;

import authentication.annotations.AuthenticatedUser;
import dao.UserDAO;
import domain.User;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

@RequestScoped
public class AuthenticatedUserProducer {

    @Inject
    private UserDAO userDAO;

    @Produces
    @RequestScoped
    @AuthenticatedUser
    private User authenticatedUser;

    /**
     * handles the observing of the Authenticated user.
     * @param userName
     */
    public void handleAuthenticationEvent(@Observes @AuthenticatedUser String userName) {
        authenticatedUser = userDAO.findByName(userName);
    }
}