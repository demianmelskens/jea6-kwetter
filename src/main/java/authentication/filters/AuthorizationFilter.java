package authentication.filters;

import authentication.annotations.AuthenticatedUser;
import authentication.annotations.Secured;
import util.enums.UserType;
import domain.User;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Secured
@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Inject
    @AuthenticatedUser
    private User authenticatedUser;

    /**
     * Checks the authorization right
     * if their not authorized then return 403 FORBIDDEN
     * else go on.
     * @param containerRequestContext
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {

        // Get the resource class which matches with the requested URL
        // Extract the roles declared by it
        Class<?> resourceClass = resourceInfo.getResourceClass();
        List<UserType> classRoles = extractRoles(resourceClass);

        // Get the resource method which matches with the requested URL
        // Extract the roles declared by it
        Method resourceMethod = resourceInfo.getResourceMethod();
        List<UserType> methodRoles = extractRoles(resourceMethod);

        try {

            // Check if the user is allowed to execute the method
            // The method annotations override the class annotations
            if (methodRoles.isEmpty()) {
                checkPermissions(classRoles);
            } else {
                checkPermissions(methodRoles);
            }

        } catch (Exception e) {
            containerRequestContext.abortWith(
                    Response.status(Response.Status.FORBIDDEN).build());
        }
    }

    /**
     * Extracts the roles defined in the annotation from the methode or object.
     * if the are no roles return empty list.
     * else return list of the roles.
     * @param annotatedElement
     * @return List<UserType>
     */
    private List<UserType> extractRoles(AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return new ArrayList<>();
        } else {
            Secured secured = annotatedElement.getAnnotation(Secured.class);
            if (secured == null) {
                return new ArrayList<>();
            } else {
                UserType[] allowedRoles = secured.value();
                return Arrays.asList(allowedRoles);
            }
        }
    }

    /**
     * Checks if the user has an authorization level which is available in the permissions list.
     * @param permissions
     * @throws Exception
     */
    private void checkPermissions(List<UserType> permissions) throws Exception{
        if(!permissions.isEmpty()){
            if(!permissions.contains(authenticatedUser.getType())){
                throw new Exception();
            }
        }
    }
}