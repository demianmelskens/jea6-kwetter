package boundary.controllers;

import domain.User;
import services.UserService;
import util.converters.Hasher;
import util.enums.UserType;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

@Named
@RequestScoped
public class LoginController {

    @Inject
    private UserService userService;

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * logs in the user and return a JWT Token for further authentication
     * @return Response
     */
    public String login(){
        FacesContext context = FacesContext.getCurrentInstance();
        String pass = Hasher.stringToHash(this.username + this.password);
        User tmpUser = userService.findByName(this.username);

        if(tmpUser.getPassword().equals(pass) && tmpUser.getType().equals(UserType.ADMIN)){
            String token = userService.login(tmpUser, new Date(System.currentTimeMillis()));
            context.getExternalContext().getSessionMap().put("user", tmpUser);
            context.getExternalContext().getSessionMap().put("token", token);
            return "views/useroverview?faces-redirect=true";
        }else{
            context.addMessage(null, new FacesMessage("Unknown login, try again"));
            return null;
        }
    }

    /**
     * invalidates the session and makes sure the
     * @return String
     */
    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/login?faces-redirect=true";
    }
}
