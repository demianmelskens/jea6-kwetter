package boundary.controllers;

import domain.Tweet;
import domain.User;
import services.TweetService;
import services.UserService;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Map;

@Named
@RequestScoped
public class TweetController {

    private long userId;

    private Tweet selectedTweet;

    private Collection<Tweet> tweets;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Tweet getSelectedTweet() {
        return selectedTweet;
    }

    public void setSelectedTweet(Tweet selectedTweet) {
        this.selectedTweet = selectedTweet;
    }

    public Collection<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(Collection<Tweet> tweets) {
        this.tweets = tweets;
    }

    @Inject
    private UserService userService;

    @Inject
    private TweetService tweetService;

    /**
     * retrieves all tweets from user and saves them to the field.
     */
    public void retrieveTweets(){
        User tmpUser = userService.findUser(userId);
        this.tweets = tmpUser.getTweets();
    }

    /**
     * deletes a user from database
     */
    public void delete(){
        tweetService.remove(this.selectedTweet);
    }
}
