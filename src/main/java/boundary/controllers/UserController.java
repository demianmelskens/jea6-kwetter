package boundary.controllers;

import domain.Tweet;
import domain.User;
import services.TweetService;
import services.UserService;
import util.enums.UserType;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Map;

@Named
@RequestScoped
public class UserController {

    private long userId;
    private String name;
    private String type;
    private String bio;
    private String location;
    private String website;
    private User selectedUser;

    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    //</editor-fold>

    @Inject
    private TweetService tweetService;

    @Inject
    private UserService userService;

    /**
     * gets all users from the database
     * @return Response
     */
    public Collection<User> getAllUsers(){
        Collection<User> userlist = userService.findAllUsers();
        for (User u : userlist){
            if(u.getType() == UserType.ADMIN){
                userlist.remove(u);
                break;
            }
        }
        return userlist;
    }

    /**
     * deletes a user from database
     */
    public void delete(){
        userService.removeUser(this.selectedUser);
    }

    /**
     * retrieves data from user and sets it to selectedUser object
     */
    public void retrieveUser(){
        User tmp = userService.findUser(userId);
        this.setvariables(tmp);
    }

    /**
     * edits a user object
     * @return String
     */
    public String editUser(){
        userService.editUser(this.updateUser());
        return "useroverview?faces-redirect=true";
    }

    /**
     * sets all the controller fields
     * @param user
     */
    private void setvariables(User user){
        this.name = user.getName();
        this.type = user.getType().toString();
        this.bio= user.getBio();
        this.location = user.getLocation();
        this.website = user.getWebsite();
    }

    /**
     * retrieve user object and change values
     * @return User
     */
    private User updateUser(){
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params =
                fc.getExternalContext().getRequestParameterMap();

        User tmp = userService.findByName(params.get("username"));
        tmp.setType(UserType.valueOf(this.type));
        tmp.setBio(this.bio);
        tmp.setLocation(this.location);
        tmp.setWebsite(this.website);
        return tmp;
    }
}
