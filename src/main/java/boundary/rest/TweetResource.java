package boundary.rest;

import authentication.annotations.Secured;
import domain.Tweet;
import services.TweetService;
import services.UserService;
import util.enums.UserType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.text.ParseException;
import java.util.Date;

@Path("tweets")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class TweetResource {

    @Inject
    private TweetService tweetService;

    @Inject
    private UserService userService;

    @Context
    private UriInfo uriInfo;

    /**
     * Adds a new tweet to the database
     * @param tweet
     * @return Response
     */
    @POST
    @Secured
    public Response addTweet(Tweet tweet){
        tweet.setSender(userService.findByName(tweet.getSender().getName()));
        tweet.setPostedAt(new Date());
        return Response.ok(tweetService.createTweet(tweet)).build();
    }

    /**
     * deletes a tweet from the database based on id
     * @param id
     * @return Response
     */
    @DELETE
    @Path("/{id}")
    @Secured({UserType.MODERATOR, UserType.ADMIN})
    public Response deleteTweet(@PathParam("id") long id){
        return Response.ok(tweetService.remove(tweetService.findTweet(id))).build();
    }

    /**
     * gets a tweet from the database based on the id
     * @param id
     * @return Response
     */
    @GET
    @Path("/{id}")
    @Secured
    public Response getTweet(@PathParam("id") long id){
        return Response.ok(tweetService.findTweet(id)).build();
    }

    /**
     * gets tweets from the database based on the postedAt date
     * @param date
     * @return Response
     */
    @GET
    @Path("date/{date}")
    @Secured
    public Response getTweet(@PathParam("date") String date) throws ParseException {
        return Response.ok(tweetService.findTweetsByDate(date)).build();
    }

    /**
     * gets all the tweets from a user
     * @param id
     * @return Response
     */
    @GET
    @Path("users/{id}")
    @Secured
    public Response getTweetsForUser(@PathParam("id") long id){
        return Response.ok(tweetService.getTweetsForUser(userService.findUser(id))).build();
    }

    /**
     * gets all the tweets from the database
     * @return Response
     */
    @GET
    @Secured
    public Response getAllTweets(){
        return Response.ok(tweetService.findAllTweets()).build();
    }

    /**
     * gets all the tweets from the database
     * @return Response
     */
    @GET
    @Path("users/{id}/timeline")
    @Secured
    public Response getTimeline(@PathParam("id") long id){
        return Response.ok(tweetService.getTimelineForUser(userService.findUser(id))).build();
    }

    @PUT
    @Path("{id}/like/{userid}")
    @Secured
    public Response likeTweet(@PathParam("id") long id, @PathParam("userid") long userId){
        return Response.ok(tweetService.likeTweet(tweetService.findTweet(id), userService.findUser(userId))).build();
    }

    @PUT
    @Path("{id}/unlike/{userid}")
    @Secured
    public Response unlikeTweet(@PathParam("id") long id, @PathParam("userid") long userId){
        return Response.ok(tweetService.unlikeTweet(tweetService.findTweet(id), userService.findUser(userId))).build();
    }

    @GET
    @Path("/find")
    @Secured
    public Response findTweets(@QueryParam("text") String text){
        return Response.ok(this.tweetService.findSimilarTweets(text)).build();
    }


}
