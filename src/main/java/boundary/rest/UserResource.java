package boundary.rest;

import authentication.annotations.Secured;
import domain.Tweet;
import domain.User;
import jdk.nashorn.internal.objects.annotations.Getter;
import services.TweetService;
import services.UserService;
import util.converters.Hasher;
import util.enums.UserType;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class UserResource {

    @Inject
    private TweetService tweetService;

    @Inject
    private UserService userService;

    @Context
    private UriInfo uriInfo;

    /**
     * gets the total ammount of users in de database
     * @return int
     */
    @GET
    @Path("/count")
    public int getUserCount(){
        return userService.count();
    }

    /**
     * adds a new user to the database
     * @param user
     * @return Response
     */
    @POST
    public Response addUser(User user){
        return Response.ok(userService.createUser(user)).build();
    }


    /**
     * changes user information
     * @param user
     * @return Response
     */
    @PUT
    @Secured
    public Response editUser(String user){
        JsonReader jsonReader = Json.createReader(new StringReader(user));
        JsonObject tmp = jsonReader.readObject();
        jsonReader = Json.createReader(new StringReader(tmp.getString("data")));
        JsonObject object = jsonReader.readObject();
        jsonReader.close();
        User tmpUser = userService.findUser(object.getJsonNumber("id").longValue());
        tmpUser.setBio(object.getString("bio"));
        tmpUser.setLocation(object.getString("location"));
        tmpUser.setWebsite(object.getString("website"));
        return Response.ok(userService.editUser(tmpUser)).build();
    }

    /**
     * deletes a user from the database based on the id
     * @param id
     * @return response
     */
    @DELETE
    @Path("/{id}")
    @Secured
    public Response deleteUser(@PathParam("id") long id){
        return Response.ok(userService.removeUser(userService.findUser(id))).build();
    }

    /**
     * gets a user from the database based on the id
     * @param id id of the user
     * @return response
     */
    @GET
    @Path("/{id}")
    @Secured
    public Response getUser(@PathParam("id") long id, @Context UriInfo uriInfo){
        User user = userService.findUser(id);
        user.resetLinks();
        addSelf(uriInfo, user);
        addfollowing(uriInfo, user);
        addfollowers(uriInfo, user);
        addTweets(uriInfo, user);
        return Response.ok(user).build();
    }

    /**
     * gets a user from the database based on the name
     * @param name name of the user
     * @return Response
     */
    @GET
    @Path("name/{name}")
    @Secured
    public Response getUserByName(@PathParam("name") String name){
        return Response.ok(userService.findByName(name)).build();
    }

    /**
     * gets authenticated user
     * @return Response
     */
    @GET
    @Path("/auth")
    @Secured
    public Response getAuthenticatedUser(){
        return Response.ok(userService.getAuthenticatedUser()).build();
    }

    /**
     * gets all users from the database
     * @return Response
     */
    @GET
    @Secured
    public Response getAllUsers(){
        return Response.ok(userService.findAllUsers()).build();
    }

    /**
     * follows the user of which the id has ben provided
     * @param id id of user to follow
     * @return Response
     */
    @PUT
    @Path("/follow/{id}")
    @Secured
    public Response followUser(@PathParam("id") long id){
        return Response.ok(userService.followUser(userService.findUser(id))).build();
    }

    /**
     * unfollows the user of which the id has ben provided
     * @param id id of user to unfollow
     * @return Response
     */
    @PUT
    @Path("/unfollow/{id}")
    @Secured
    public Response unfollowUser(@PathParam("id") long id){
        return Response.ok(userService.unfollowUser(userService.findUser(id))).build();
    }

    /**
     * gets all the followers from the user based on the id
     * @param id
     * @return Response
     */
    @GET
    @Path("/{id}/followers")
    @Secured
    public Response getFollowers(@PathParam("id") long id){
        return Response.ok(userService.getFollowers(userService.findUser(id))).build();
    }

    /**
     * gets all the following from the user based on the id
     * @param id
     * @return Response
     */
    @GET
    @Path("/{id}/following")
    @Secured
    public Response getFollowing(@PathParam("id") long id){
        return Response.ok(userService.getFollowing(userService.findUser(id))).build();
    }

    /**
     * changes the rights of a user.
     * @param id
     * @param type
     * @return
     */
    @POST
    @Path("/rights")
    @Secured(UserType.ADMIN)
    public Response changeRights(@HeaderParam("id") long id, @HeaderParam("type") UserType type){
        return Response.ok(userService.changeRights(userService.findUser(id), type)).build();
    }

    /**
     * logs in the user and return a JWT Token for further authentication
     * @param username
     * @param password
     * @return Response
     */
    @POST
    @Path("/login")
    public Response login(@HeaderParam("name") String username, @HeaderParam("password") String password){
        String pass = Hasher.stringToHash(username + password);
        User tmpUser = userService.findByName(username);
        if(tmpUser.getPassword().equals(pass)){
            String token = userService.login(tmpUser, new Date(System.currentTimeMillis()));
            JsonObject jsonToken = Json.createObjectBuilder().add("token", token).build();
            return Response.ok(jsonToken, MediaType.APPLICATION_JSON).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    @GET
    @Path("/find")
    @Secured
    public Response findUsers(@QueryParam("name") String name){
        return Response.ok(this.userService.findSimilarUsers(name)).build();
    }

    private void addSelf(UriInfo uriInfo, User user){
        String uri = uriInfo.getBaseUriBuilder()
                .path(UserResource.class)
                .path(Long.toString(user.getId()))
                .build()
                .toString();
        user.addLink(uri, "self");
    }

    private void addfollowing(UriInfo uriInfo, User user){
        String uri = uriInfo.getBaseUriBuilder()
                .path(UserResource.class)
                .path(UserResource.class, "getFollowing")
                .resolveTemplate("id", user.getId())
                .build()
                .toString();
        user.addLink(uri, "following");
    }

    private void addfollowers(UriInfo uriInfo, User user){
        String uri = uriInfo.getBaseUriBuilder()
                .path(UserResource.class)
                .path(UserResource.class, "getFollowers")
                .resolveTemplate("id", user.getId())
                .build()
                .toString();
        user.addLink(uri, "followers");
    }

    private void addTweets(UriInfo uriInfo, User user){
        String uri = uriInfo.getBaseUriBuilder()
                .path(TweetResource.class)
                .path(UserResource.class)
                .path(Long.toString(user.getId()))
                .build()
                .toString();
        user.addLink(uri, "tweets");
    }
}
