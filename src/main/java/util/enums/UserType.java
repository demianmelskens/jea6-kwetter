package util.enums;

/**
 * used to assign user rights.
 */
public enum UserType {
    NORMAL,
    MODERATOR,
    ADMIN
}
