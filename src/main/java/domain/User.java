package domain;

import util.converters.Hasher;
import util.enums.UserType;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Demian
 */
@Entity
@Table(name = "user")
@NamedQueries({ @NamedQuery(name="User.count", query="SELECT COUNT(u) FROM User u"),
                @NamedQuery(name="User.findByName", query="SELECT u FROM User u WHERE u.name = :name"),
                @NamedQuery(name="User.findAll", query="SELECT u FROM User u"),
                @NamedQuery(name="User.findSimilar", query="SELECT u FROM User u WHERE u.name LIKE :name")
})
public class User{
    private static final long serialVersionUID = 1L;

    //private fields
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", unique = true, length = 50)
    private String name;        //name of the user

    @Column(name = "password", length = 256, nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private UserType type;      //type of user

    @Column(name = "bio", length = 160)
    private String bio;         //biography that has been defined by the user.

    @Column(name = "location")
    private String location;    //??

    @Column(name = "website")
    private String website;     //website link

    @ManyToMany
    @JoinTable(name = "following",
            joinColumns = @JoinColumn(name = "followed_id"),
            inverseJoinColumns = @JoinColumn(name = "follower_id"))
    @JsonbTransient
    private Collection<User> followers;     //all users that are following the user.

    @ManyToMany(mappedBy = "followers")
    @JsonbTransient
    private Collection<User> following;     //all users that are following the user.

    @OneToMany(mappedBy = "sender", cascade = CascadeType.PERSIST, orphanRemoval = true)
    @JsonbTransient
    private Collection<Tweet> tweets;      //all tweets send by the user.

    @ManyToMany(mappedBy = "likes", cascade = CascadeType.PERSIST)
    @JsonbTransient
    private Collection<Tweet> liked;

    @Transient
    private Collection<Link> links;

    //constructors
    /**
     * non args constructor
     */
    public User() {
        this.following = new ArrayList<User>();
        this.followers = new ArrayList<User>();
        this.tweets = new ArrayList<Tweet>();
        this.liked = new ArrayList<Tweet>();
        this.links = new ArrayList<Link>();
        this.type = UserType.NORMAL;
    }

    /**
     * Constructor
     * @param name username
     * @param bio biography
     * @param location location
     * @param website website link
     */
    public User(String name, String bio, String location, String website) {
        this.name = name;
        this.password = Hasher.stringToHash(name + password); //mogelijk wordt dit verplaatst naar de front-end
        this.type = UserType.NORMAL;
        this.bio = bio;
        this.location = location;
        this.website = website;
        this.following = new ArrayList<User>();
        this.followers = new ArrayList<User>();
        this.tweets = new ArrayList<Tweet>();
        this.links = new ArrayList<Link>();
    }

    /**
     * Constructor
     * @param name username
     * @param password password
     * @param bio biography
     * @param location location
     * @param website website link
     */
    public User(String name, String password, String bio, String location, String website) {
        this.name = name;
        this.password = Hasher.stringToHash(name + password);
        this.type = UserType.NORMAL;
        this.bio = bio;
        this.location = location;
        this.website = website;
        this.following = new ArrayList<User>();
        this.followers = new ArrayList<User>();
        this.tweets = new ArrayList<Tweet>();
        this.links = new ArrayList<Link>();
    }

    //getters and setters
    public long getId(){
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonbTransient
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = Hasher.stringToHash(this.name + password);
    }

    @JsonbTransient
    public UserType getType(){
        return this.type;
    }

    public void setType(UserType type){
        this.type = type;
    }

    public boolean isAdmin(){
        return this.type == UserType.ADMIN;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Collection<User> getFollowers() {
        return followers;
    }

    public void setFollowers(Collection<User> followers) {
        this.followers = followers;
    }

    public void addFollowers(User user){
        this.followers.add(user);
    }

    public void removeFollowers(User user){
        this.followers.remove(user);
    }

    public Collection<User> getFollowing() {
        return following;
    }

    public void setFollowing(Collection<User> following) {
        this.following = following;
    }

    public void addFollowing(User user){
        this.following.add(user);
    }

    public void removeFollowing(User user){
        this.following.remove(user);
    }

    public Collection<Tweet> getTweets() {
        return tweets;
    }

    public void setTweets(Collection<Tweet> tweets) {
        this.tweets = tweets;
    }

    public Collection<Tweet> getLiked() {
        return liked;
    }

    public void setLiked(Collection<Tweet> liked) {
        this.liked = liked;
    }

    public void addLiked(Tweet tweet){
        this.liked.add(tweet);
    }

    public void removeLiked(Tweet tweet){
        this.liked.remove(tweet);
    }

    public Collection<Link> getLinks() {
        return links;
    }

    public void setLinks(Collection<Link> links) {
        this.links = links;
    }

    public void addLink(String link, String rel){
        Link linkObject = new Link(link, rel);
        this.links.add(linkObject);
    }

    public void removeLink(Link link){
        this.links.remove(link);
    }

    public void resetLinks(){
        this.links = new ArrayList<Link>();
    }
}
