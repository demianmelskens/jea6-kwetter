package domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author Demian
 */
@Entity
@Table(name = "Tweet")
@NamedQueries({ @NamedQuery(name="Tweet.findById", query="SELECT t FROM Tweet t WHERE t.id = :id"),
                @NamedQuery(name="Tweet.findAll", query="SELECT t FROM Tweet t"),
                @NamedQuery(name="Tweet.findByDate", query="SELECT t FROM Tweet t WHERE t.postedAt = :date"),
                @NamedQuery(name = "Tweet.getTweetsForUser", query = "SELECT t FROM Tweet t WHERE t.sender = :id"),
                @NamedQuery(name = "Tweet.findSimilar", query = "SELECT t FROM Tweet t WHERE t.tweet LIKE :text")
})
public class Tweet{
    private static final long serialVersionUID = 1L;

    //private fields
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "tweet", length = 140)
    private String tweet;

    @ManyToOne()
    @JoinColumn(name = "sender")
    private User sender;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "postedAt")
    private Date postedAt;

    @ManyToMany
    @JoinTable(name="Likes",
            joinColumns=@JoinColumn(name="tweet_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="user_id", referencedColumnName="id"))
    private Collection<User> likes = new ArrayList<User>();

    //constructors

    /**
     * NoParam constructor
     */
    public Tweet() {
    }

    /**
     * Standard tweets constructor
     * @param tweet
     * @param sender
     */
    public Tweet(String tweet, User sender){
        this.tweet = tweet;
        this.sender = sender;
        this.postedAt = new Date();
    }

    /**
     * Standard tweets constructor
     * @param tweet
     * @param sender
     * @param postedAt
     */
    public Tweet(String tweet, User sender, Date postedAt){
        this.tweet = tweet;
        this.sender = sender;
        this.postedAt = postedAt;
    }

    //getters and setters
    public long getId(){
        return this.id;
    }

    public String getTweet() {
        return this.tweet;
    }

    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    public User getSender() {
        return this.sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Date getPostedAt() {
        return this.postedAt;
    }

    public void setPostedAt(Date postedAt) {
        this.postedAt = postedAt;
    }

    public Collection<User> getLikes() {
        return likes;
    }

    public void setLikes(Collection<User> likes) {
        this.likes = likes;
    }

    public void addLike(User user){
        this.likes.add(user);
    }

    public void removeLike(User user){
        this.likes.remove(user);
    }
}
