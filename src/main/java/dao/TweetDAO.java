package dao;

import domain.Tweet;
import domain.User;

import java.util.Collection;
import java.util.Date;

/**
 * @author Demian
 */
public interface TweetDAO {

    Tweet create(Tweet tweet);

    Tweet remove(Tweet tweet);

    Tweet find(long id);

    Collection<Tweet> findByDate(Date date);

    Collection<Tweet> getTweetsForUser(User user);

    Collection<Tweet> findAll();

    Collection<Tweet> getTimelineForUser(User user);

    Tweet like(Tweet tweet, User user);

    Tweet unlike(Tweet tweet, User user);

    Collection<Tweet> findSimilarTweets(String text);
}
