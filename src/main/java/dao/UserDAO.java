package dao;

import domain.Tweet;
import domain.User;

import java.util.Collection;

/**
 * @author Demian
 */
public interface UserDAO {
    int count();

    User create(User user);

    User edit(User user);

    User remove(User user);

    User find(Long id);

    User findByName(String username);

    Collection<User> findAll();

    User followUser(User user1, User user2);

    User unfollowUser(User user1, User user2);

    Collection<User> getFollowers(User user);

    Collection<User> getFollowing(User user);

    Collection<User> findSimilarUsers(String name);
}
