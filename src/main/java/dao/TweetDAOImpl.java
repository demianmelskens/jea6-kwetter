package dao;

import domain.Tweet;
import domain.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@Stateless
public class TweetDAOImpl implements TweetDAO {

    @PersistenceContext
    EntityManager em;

    /**
     * Adds a new tweet to the database
     * @param tweet
     * @return Tweet
     */
    public Tweet create(Tweet tweet) {
        em.persist(tweet);
        return tweet;
    }

    /**
     * updates tweet;
     * @param tweet
     * @return
     */
    public Tweet edit(Tweet tweet){
        em.merge(tweet);
        return tweet;
    }

    /**
     * deletes a tweet from the database based on id
     * @param tweet
     * @return Tweet
     */
    public Tweet remove(Tweet tweet){
        if (!em.contains(tweet)) {
            tweet = em.merge(tweet);
        }
        em.remove(tweet);
        return tweet;
    }

    /**
     * gets a tweet from the database based on the id
     * @param id
     * @return Tweet
     */
    public Tweet find(long id) {
        try {
            return em.createNamedQuery("Tweet.findById", Tweet.class).setParameter("id", id).getSingleResult();
        }catch (Exception ex){
            return null;
        }
    }

    /**
     * gets tweets from the database based on the postedAt date
     * @param date
     * @return Collection<Tweet>
     */
    public Collection<Tweet> findByDate(Date date) {
        try {
            return em.createNamedQuery("Tweet.findByDate", Tweet.class).setParameter("date", date, TemporalType.DATE).getResultList();
        }catch (Exception ex){
            return Collections.emptyList();
        }
    }

    /**
     * gets all the tweets from a user
     * @param user
     * @return Collection<Tweet>
     */
    public Collection<Tweet> getTweetsForUser(User user) {
        try {
            return em.createNamedQuery("Tweet.getTweetsForUser", Tweet.class).setParameter("id", user).getResultList();
        }catch (Exception ex){
            return Collections.emptyList();
        }
    }

    /**
     * gets all the tweets from the database
     * @return Collection<Tweet>
     */
    public Collection<Tweet> findAll() {
        try {
            return em.createNamedQuery("Tweet.findAll", Tweet.class).getResultList();
        }catch (Exception ex){
            return Collections.emptyList();
        }
    }

    /**
     * gets all the tweets in the timeline from the user
     * @param user
     * @return Collection<Tweet>
     */
    public Collection<Tweet> getTimelineForUser(User user) {
        Collection<Tweet> timeline = new ArrayList<Tweet>();
        Collection<User> following = user.getFollowing();
        for (User u: following) {
            timeline.addAll(getTweetsForUser(u));
        }
        return timeline;
    }

    /**
     * like a tweet
     * @param tweet
     * @param user
     * @return Tweet
     */
    public Tweet like(Tweet tweet, User user) {
        tweet.addLike(user);
        user.addLiked(tweet);
        em.merge(tweet);
        em.merge(user);
        return tweet;
    }

    /**
     * unlike a tweet
     * @param tweet
     * @param user
     * @return Tweet
     */
    public Tweet unlike(Tweet tweet, User user) {
        tweet.removeLike(user);
        user.removeLiked(tweet);
        em.merge(tweet);
        em.merge(user);
        return tweet;
    }

    @Override
    public Collection<Tweet> findSimilarTweets(String text) {
        return em.createNamedQuery("Tweet.findSimilar", Tweet.class).setParameter("text", "%" + text + "%").getResultList();
    }
}
