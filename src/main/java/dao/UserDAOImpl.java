package dao;

import domain.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Collections;

@Stateless
public class UserDAOImpl implements UserDAO {

    @PersistenceContext
    EntityManager em;

    /**
     * gets the total ammount of users in de database
     * @return int
     */
    public int count() {
        try {
            return em.createNamedQuery("User.count", Long.class).getSingleResult().intValue();
        }catch (Exception ex){
            return 0;
        }
    }

    /**
     * adds a new user to the database
     * @param user
     * @return User
     */
    public User create(User user) {
        em.persist(user);
        return user;
    }

    /**
     * changes user information
     * @param user
     * @return User
     */
    public User edit(User user) {
        em.merge(user);
        return user;
    }

    /**
     * deletes a user from the database based on the id
     * @param user
     * @return User
     */
    public User remove(User user) {
        if (!em.contains(user)) {
            user = em.merge(user);
        }
        em.remove(user);
        return user;
    }

    /**
     * gets a user from the database based on the id
     * @param id
     * @return User
     */
    public User find(Long id) {
        return em.find(User.class, id);
    }

    /**
     * gets a user from the database based on the name
     * @param name
     * @return User
     */
    public User findByName(String name) {
        try {
            return em.createNamedQuery("User.findByName", User.class).setParameter("name", name).getSingleResult();
        }catch (Exception ex){
            return null;
        }
    }

    /**
     * gets all users from the database
     * @return Collection<User>
     */
    public Collection<User> findAll() {
        try {
            return em.createNamedQuery("User.findAll", User.class).getResultList();
        }catch (Exception ex){
            return Collections.emptyList();
        }
    }

    /**
     * adds a user to the following list
     * @param user1
     * @param user2
     * @return User
     */
    public User followUser(User user1, User user2) {
        user1 = em.find(User.class, user1.getId());
        user2 = em.find(User.class, user2.getId());
        user1.addFollowing(user2);
        user2.addFollowers(user1);
        em.merge(user1);
        em.merge(user2);
        return user2;
    }

    /**
     * removes a user from the following list
     * @param user1
     * @param user2
     * @return User
     */
    public User unfollowUser(User user1, User user2) {
        user1 = em.find(User.class, user1.getId());
        user2 = em.find(User.class, user2.getId());
        user1.removeFollowing(user2);
        user2.removeFollowers(user1);
        em.merge(user1);
        em.merge(user2);
        return user2;
    }

    /**
     * gets all the followers of the user
     * @param user
     * @return Collection<User>
     */
    public Collection<User> getFollowers(User user) {
        return user.getFollowers();
    }

    /**
     * get all the following of the user
     * @param user
     * @return Collection<User>
     */
    public Collection<User> getFollowing(User user) {
        return user.getFollowing();
    }

    /**
     * gets all the users which name look like input string
     * @param name
     * @return
     */
    public Collection<User> findSimilarUsers(String name) {
        return em.createNamedQuery("User.findSimilar", User.class).setParameter("name", "%"+name+"%").getResultList();
    }


}
